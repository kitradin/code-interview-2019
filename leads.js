$(document).ready(function(){
    let data;

    $.get('https://api.myjson.com/bins/unxs8', function(data){ console.log(data)
        $('#all-leads .value').text(data.all)
        $('#nonqual .value').text(data.nonQual)
        $('#qual .value').text(data.qual)

        displayLeads(data.leads);
    }, "json")

    // sorting function on click
    $('#leads .asc').click(function(evt){
        const column = $(evt.target).parent().attr('class');
        const sorted_leads = sortLeads(data.leads, column, dir)
        displayLeads(sorted_leads);
    });

    function sortLeads(leads, column, dir){

    }

    function displayLeads(leads){
        leads.forEach(function(item){
            $('ul#leads').append(
                '<li>' +
                    '<ul>' +
                        '<li class="num">'+item.id+'</li>' +
                        '<li class="date">'+item.date+'</li>' +
                        '<li class="first">'+item.firstName+'</li>' +
                        '<li class="last">'+item.lastName+'</li>' +
                        '<li class="age">'+item.age+'</li>' +
                        '<li class="value">'+item.value+'</li>' +
                        '<li class="mortgage">'+item.mortgage+'</li>' +
                        '<li class="state">'+item.state+'</li>' +
                        '<li class="quality">'+item.dispo+'</li>' +
                        '<li class="more"><button>More +</button></li>' +
                    '</ul>' +
                '</li>'
            )
        })
    }

});